import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Ship from './views/Ship.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/:route_page',
      name: 'home',
      component: Home,
      props: true,
    },
    {
      path: '/ship/:id',
      name: 'ship',
      component: Ship,
      props: true,
    },
  ],
});
